# Pátek's graphics

All of the contents of this repo are licensed under The Unlicense, however **keep in mind they are a part of our visual identity and therefore their usage may be limited both ethically and legally**. Feel free to contact us on [patek@gbl.cz](mailto:patek@gbl.cz) to consult your intended usage.

## List of the contents

### Our colors

The main blue-ish color is #0b2a37 / RGB (11, 42, 55) / CMYK (80, 24, 0, 78) / Pantone 2189-C.

### Logo

Our logo is available as an [HTML custom element](https://gitlab.com/patek-devs/patek-logo-custom-element).

### Stamp

You can find a [Pátek stamp](./Pátek-stamp.FCStd) in this repo. Useful for branding cloth.

## The Unlicense

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
